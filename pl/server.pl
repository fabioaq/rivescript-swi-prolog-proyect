/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

%Modules
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_files)).

:- multifile http:location/3.
http:location(files, root(files), []).
user:file_search_path(folders, library('images/styles/scripts')).

/**
 * server(+Message:String, ?R:String)
 *
 * Recibe el mensaje escrito por el usuario, envía peticion
 * http post y devuelve el response en R.   
  */
server(Port) :-
        http_server(http_dispatch, [port(Port)]).

:- http_handler(root(.), http_reply_from_files('.', []), [prefix]). % defaults to index.html
:- http_handler(files(.), serve_files_in_directory(folders), [prefix]).

stop_server(Port) :-
    http_stop_server(Port, []).
