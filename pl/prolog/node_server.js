/**
 * @file Archivo js/rivescript
 * @author Lisandra Agüero Ruiz
 * @author Esteban Espinoza Fallas
 * @author Steve Díaz Reyes
 * @author José Fabio Alfaro Quesada 
 * @name node_server.js
 */
/**
 * @type {http}
 */
var http = require("http");
/**
 * @type {String}
 */
const RiveScript = require('./js/rivescript');
/**
 * @type {int}
 */
const port = 8888;
/**
 * @type {string}
 */
const hostname = '127.0.0.1';
/**
 * @type {string}
 */
let user_msg;
/**
 * @type {RiveScript}
 */
let bot = new RiveScript({ utf8: true });
bot.unicodePunctuation = new RegExp(/[.,!?;:]/g);
/**
 * @type {string}
 */
const username = "local-user";
/**
 * @type {string}
 */
let RIVE_FILE;
/*-------------------------------------------------------------------------------------------*/
/** 
 * Inicilizacion del server, se ejecuta cada vez que llega un request 
 */
const server = http.createServer((req, res) => {
    bot = new RiveScript({ utf8: true });
    let data = []
    req.on('data', chunk => {
        data.push(chunk)
    })
    req.on('end', () => {
        user_msg = parse_data(data) // JSON.parse(data) MENSAJE DEL USUARIO
        console.log("Msj recibido: " + user_msg.msg)
        init_bot(
            () => {
                reply(user_msg, (bot_resp) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'text/plain;charset=utf-8');
                    res.end(bot_resp);
                })
            }
        )
    })
});

/** 
 * Devuelve un Json
 * @Param {String} data Informacion del request
 */
function parse_data(data) {
    try {
        return JSON.parse(data)
    } catch (e) {
        console.log(`Invalid Message: ${e.message}`)
        return { msg: `"*** ${e.message} ***"`, err: true }
    }
}

/** 
 * Iniciliza el nombre del archivo a leer para buscar las respuestas a un chat y 
 * carga el archivo al bot
 * @Param {String} reply_to_client respuesta del bot al usuario
 * 
 */
function init_bot(reply_to_client) {

    RIVE_FILE = "./rivescripts/" + user_msg.username + ".rive";
    console.log("Archivo leído: " + RIVE_FILE);
    // Create bot
    bot.loadFile(RIVE_FILE)
        .then(loading_done)
        .then(reply_to_client)
        .catch(loading_error)
}

/** 
 * una vez el bot se haya inicilizado se hace un acomodo de las respuestas en los buffer
 * 
 */
function loading_done() {
    //console.log("Bot has finished loading!");
    bot.sortReplies();
}

/** 
 * muestra mensaje de error si hay un fallo durante el proceso
 * @Param {String} error error generado 
 * 
 */
function loading_error(error, filename, lineno) {
    console.log("Error when loading files: " + error);
}

/** 
 * Devuelve la respuesta del bot al usuario
 * 
 */
function reply(msg, cb) {
    if (!bot || msg.err) {
        cb(msg.msg)
        return
    }
    bot.sortReplies();
    bot.reply(username, msg.msg).then((resp) => {
        console.log("The bot says: " + resp);
        console.log("----------------------------------------")
        cb(resp)
    });
}
/** 
 * pone a escuchar al servidor en el puerto definido
 * @Param {String} port puerto a definir para usar el servidor
 * @Param {String} hostname dirección local host
 * 
 */
server.listen(port, hostname, () => {
    console.clear()
    console.log(`Server running at http://${hostname}:${port}/`);
    console.log("--------------------------------------------------")
});