/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/
:- use_module('compilador/src/rs.compile/rsCompiler').
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_files)).
:- use_module(library(http/websocket)).
:- use_module(library(http/json_convert)).
:- use_module('compilador/src/rs.eval/rsEval').
:- encoding(utf8).


:- use_module(library(http/http_client)).

node_endpoint('http://localhost:8888').

% websocket sintax check handler
:- http_handler(root(postmsg), 
                http_upgrade_to_websocket(post_msg, []),
                [spawn([])])
.

:- http_handler(root(prueba), 
                http_upgrade_to_websocket(probando, []),
                [spawn([])])
.

:- initialization(start_server)
.

:- use_module(library(http/http_json)).

:- http_handler(root(test), 
                http_upgrade_to_websocket(test_handler, []),
                [spawn([])]).

/**
 * test_handler(+Request:String)
 *
 * A continuación se describe el flujo del predicado...
 1. se debe recibir un JSON el cual está compuesto por el nombre del bot y el bloque.
 2. mediante get_response se extraen los datos del JSON por separado.
 3. luego el ResponseBody se pasa a un stream mediante el predicado open_string.
 4. rsCompiler:compile se encarga de tomar el Stream, el path donde se guardará el
 *  nuevo bot con extensión .rive y ResponseName como nombre del bot.
 5. rsCompiler:compile ejecutará a rsParser y rsEmiter.
  */
test_handler(Request) :-
    ws_receive(Request, Message, [format(json)]),
	(   Message.opcode == close
    ->  true
    ;  	
	  get_response(Message.data, ResponseBody, ResponseName),
	  open_string(ResponseBody, Stream),
	  rsCompiler:compile(Stream,'./rivescripts/',ResponseName),
	  ws_send(Request, json(ResponseBody)),
	  test_handler(Request)
    )
	.
% Predicado para extraer datos del JSON	
get_response(Message, ResponseBody, ResponseName) :-
  ResponseBody = Message.brain_body, ResponseName= Message.brain_name .
  
% ----------------------------------------------------------

:- http_handler(root(files), 
                http_upgrade_to_websocket(files_handler, []),
                [spawn([])]).

/**
 * files_handler(+Request:String)
 *
 * se encarga de enviar el nombre de todos los bots contenidos en la carpeta riverscript.
 * Para luegos, estos ser enviados al cliente el cual actualizará la lista de bot al iniciar sesión y cada vez
 * que el administrador ingrese un nuevo bot al sistema.   
  */
files_handler(Request) :-
    ws_receive(Request, Message),
	(   Message.opcode == close
    ->  true
    ;  	
	directory_files('./rivescripts',L),
	append(['.','..'],R,L),
	ws_send(Request, json(R)),
	files_handler(Request)
    )
	.


% ----------------------------------------------------------




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% POST MSG TO NODE SERVER %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/**
 * probando(+Request:String)
 *
 * Recibe mensaje digitado por parte del usuario y lo prepara para poder ser enviado 
 * por medio de una petición post.
 *   
  */
probando(Request) :-
    ws_receive(Request, Message, [format(json)]),
	(   Message.opcode == close
    ->  true
    ;  	
	  %get_response2(Message.data, Response),
      evaluarMsj(Message.data, Response),
      writeln(Response),
      ws_send(Request, json(Response)),
	  probando(Request)
    )
	.

/**
 * get_response2(+Message:String, ?Response:String)
 *
 * Obtiene los datos del request, y los envía por http post al servidor RS/JS   
  */
get_response2(Message, Response) :- 
  post_msg(Message.username, Message.msg, R), %writeln("Response" + Response),
  Response = R.

evaluarMsj(Message, Response) :- 
rsEval:eval(Message.msg, Response)
.
/**
 * post_msg(+Message:String, ?R:String)
 *
 * Recibe el mensaje escrito por el usuario, envía peticion
 * http post y devuelve el response en R.   
  */
post_msg(AtomMsg, Reply) :-
    node_endpoint(NodeEndPoint),
    http_post(NodeEndPoint, atom(AtomMsg), Reply, [])
. 

/**
 * post_msg(+User:String, ?Msg:String, -Reply)
 *
 * Recibe el nombre del usuario que digitó un msj, el msj y hace una petición http post al servidor 
 * de Rivescript/Js
 *   
  */
post_msg(User, Msg, Reply) :-
    writeln("User: " + User + ", Msg: " + Msg),
    node_endpoint(NodeEndPoint),
    format(atom(AtomMsg), '{"username":"~s","msg":"~s"}', [User, Msg]),
    http_post(NodeEndPoint, atom(AtomMsg), Reply, [])
. 

%%%%%%%%%%%%%%%%%%%%% Configure handlers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% static files handler
:- http_handler(root(.),
                http_reply_from_files('.', []),
                [prefix])
.
% websocket echo handler
:- http_handler(root(echo), 
                http_upgrade_to_websocket(echo, []),
                [spawn([])])
.
% Message is a dict see https://www.swi-prolog.org/pldoc/doc_for?object=ws_receive/2
    
echo(WebSocket) :-
    ws_receive(WebSocket, Message),
	(   Message.opcode == close
    ->  true
    ;   ws_send(WebSocket, Message),
        echo(WebSocket)
    ) 
. 

% websocket login handler
:- http_handler(root(login), 
                http_upgrade_to_websocket(login, []),
                [spawn([])])
.

/**
 * login(+Request:String)
 *
 * Recibe un JSON con usuario y contraseña que se quiere buscar en la base de datos. 
 *   
  */
  login(Request) :-
    ws_receive(Request, Message, [format(json)]),
	(   Message.opcode == close
    ->  true
    ;  	
    check_user(Message.data, Response),
      ws_send(Request, json(Response)),
	  login(Request)
    )
. 

/**
 * check_user(+Request:String)
 *
 * recibe la data para buscar en la base de datos y devuelve un json
 *   
  */
  check_user(Data, Json) :- 
    abrir_conexion,
    obtener_resgistro(Data.id, Data.pass, Json),
    writeln(Json),
    cerrar_conexion
. 

% DATABASE CONECTION PREDICATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Database Control %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**
 * odbc_connect(+DSN, -Connection, +Options)
 *
 * abrir una nueva conexión a mysql
 *   
  */
abrir_conexion :- 
    odbc_connect('MSProlog',_,
                    [
                        user(root),
                        password(root),
                        alias(prueba),
                        open(once)
                    ]
                ) ->
                writeln('Conection success');
                writeln('Conection denied')
. 

/**
 * odbc_disconnect(+DSN, -Connection, +Options)
 *
 * Para cerrar la conexion a MySQL previa hecha
 *   
  */
cerrar_conexion :- 
    odbc_disconnect('prueba') ->
    writeln('Conection ends');
    writeln('Conection continue')
. 

% LOGIN PREDICATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Login Control %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% crea un json_object usuario el cual almacenara la información recuperada de la base de datos.
:- json_object 
        usuario(id:integer, pass:atom, nombre:atom, rol:integer)
. 

% crea un json_object respuesta el cual contiene un status de respuesta, el usuario, y un mensaje.
:- json_object 
        respusta(status:atom, data:usuario/4 ,msg:atom)
. 

% crea un json_object user_not_found el cual contiene un status de respuesta y un mensaje.
:- json_object 
        user_not_found(status:atom, msg:atom)
. 

/**
 * obtener_resgistro(+Cedula:String, +Clave, +JSON_Object:String)
 *
 * Recibe un numero de cedula y una clave devuelve un JSON
 *   
  */
obtener_resgistro(Cedula, Clave, JSON_Object) :-
    format(string(Query), 'SELECT * FROM usuarios WHERE id = "~w" and pass = "~w" ;', [Cedula, Clave]),
    my_odbc_query('prueba',Query,Json_user),
    prolog_to_json(Json_user, JSON_Object)
. 

/**
 * my_odbc_query(+Base:String, +Query, +JSON_Object:String)
 *
 * Recibe un alias de la base a consultar y su Query, devuelve un JSON de respuesta
 *   
  */
my_odbc_query(Base,Query,Json):-
    odbc_query(Base, Query, row(Id, Pass, Nombre, Rol)),!,
    Json_user = usuario(Id, Pass, Nombre, Rol),
    Json = respusta('OK', Json_user ,'Login correcto').


% Recive un alias de la base a consultar y su Query, devuelve un JSON de error
my_odbc_query(_,_,Json):- 
    Json = user_not_found('NOK', 'Datos incorrectos')
.

% REPLY PREDICATE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Server Control %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
start_server :-
    default_port(Port),
    start_server(Port).

start_server(Port) :-
    http_server(http_dispatch, [port(Port)]).

stop_server() :-
    default_port(Port),
    stop_server(Port).

stop_server(Port) :-
    http_stop_server(Port, []).

default_port(3000).
