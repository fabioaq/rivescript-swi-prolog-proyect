/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

%:- use_module(rive_pl).
:- module(rsEval, [eval/2, cargar_bot/1]).
:- consult('../../../rivescripts/ZZZ.out.pl').

/*
 * matcher
*/
eval(Msg, Resp) :-
  atomic_list_concat(L, ' ', Msg),
  get_trigger(X, L), !,
  get_answer(X, Resp)
. 

/**
 * Cuando pega un trigger con star, entra aqui
*/
/*eval(Msg, Resp) :-
  atomic_list_concat(L, ' ', Msg),
  get_trigger(X, L), !,
  get_answer(X, Resp)
. 
*/

/**
 * Cuando no pega ningun trigger, entra aqui
*/
eval(_, 'ERR: No default topic random was found!').

/**
 * Calcular el valor de star
 * Compara un trigger con un msj y asi averigua el valor de star 
 * + Mi nombre es * ---> - hola como estas <star> 
*/

calcular_star(LT, LR, LM) :- writeln(LT + LR + LM)
.

/*
eval2(Msg) :-
  atomic_list_concat(L, ' ', Msg),
  trigger(X, L), !,
  get_answer(X, Resp),
  % buscar todos los trigger que tenga el mismo tamaño 
  atomic_list_concat(L2, ' ', Resp), 
  atomic_list_concat(LMsg, ' ', Msg), 
  %calcular_star(L, L2, LMsg),
  writeln(L), 
  writeln(L2),
  writeln(LMsg)
. 
*/
/**
 * Devuelve una respuesta random para T trigger
 * T es el id del trigger para el cual se busca respuesta
*/
get_answer(T, R) :-
  cant_R_X_T(T, Cant),
  Cant > 0 -> random_between(1, Cant, N),response(T, N, R2), !,atomic_list_concat(R2, ' ', R)
  .

/**
 * trigger sin respuesta
*/
get_answer(_, 'ERR: No Reply Found').

/**
 * Devuelve el peso total de un conjunto de respuestas para el trigger T
*/
get_answers_weight(T,R) :-
   findall(X, rsEval:response_weight(T, _ ,X), L),
   foldl([X, Y, Z] >> (Z is X + Y), L, 0, R)
.
/*
prueba(Msg, Resp) :-
  atomic_list_concat(L, ' ', Msg),
  get_trigger(X, L), !,
  get_answers_weight(X, Num), writeln(X),
  Num > 0 -> writeln(tiene); writeln(X), get_answer(X, Resp), !
. 

prueba(_, 'ERR: No default topic random was found!').
*/
/**
 * devuelve en T el numero de trigger que pega
*/
get_trigger(T, Msj) :- trigger(T, (Msj)), !
.

/**
 * devuelve el id del trigger que es de la manera + *
*/
get_trigger(T, _) :- hay_star(X) -> T = X
.

/**
 * si no pega ningun trigger
*/
get_trigger(_, 'ERR: No default topic random was found!')
.

/**
 * busca si hay un trigger que sea de la manera + *
*/
hay_star(X) :- trigger(X, ['star(1)'])
.

/**
 * cargar bot
*/
cargar_bot(Nombre) :- 
    %atom_concat(Nombre, '_vars', Variables), consult(Variables),
    atom_concat(Nombre, '_brain', Brain), consult(Brain)
.     



