/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/
:- module(rsGenerator,[
        genRiveToFile/1
    ]).

:-  use_module(rsExtractor).
/**
 * genRiveToFile(-File:String)
 * 
 * encargado de guardar dolo lo que esta en memoria para 
 */
genRiveToFile(File) :- 
    rsExtractor:delete_word,
    writeln('antes de abrir para escribir'+ File),
    open(File, write, Out), writeln(Out),
    rsExtractor:select_cant_R_X_T(R1),reverse(R1, R11), writeln(R11),
    genRiveTr_x_Res(Out,R1),
    rsExtractor:all_trigger(R2),reverse(R2, R22), writeln(R22),
    genRiveTriggers(Out,R2),
    rsExtractor:all_response(R3),reverse(R3, R33), writeln(R33),
    genRiveResponces(Out,R3),
    close(Out)
. 

/**
 * genRiveTr_x_Res(-Out:Stream ,L:List)
 * 
 * guarda la cantidad de responces por trigger
 */
genRiveTr_x_Res(_,[]).
genRiveTr_x_Res(Out,[H|B]):-
    spitter(Out, H),
    writeln(H),
    genRiveTr_x_Res(Out,B)
. 

/**
 * genRiveTriggers(-Out:Stream ,L:List)
 * 
 * guarda todos los trigger
 */
genRiveTriggers(_,[]).
genRiveTriggers(Out,[H|B]):-
    spitter(Out, H),
    writeln(H),
    genRiveTriggers(Out,B)
. 

/**
* genRiveResponces(-Out:Stream)
 * 
 * guarda todos los responces
 */
genRiveResponces(_,[]).
genRiveResponces(Out,[H|B]):-
    spitter(Out, H),
    writeln(H),
    genRiveResponces(Out,B)
. 

/**
* spitter(-Out:Stream, +Te:Atom)
 * 
 * escribe en el archivo
 */
spitter(Out, Te):- 
    write(Out, Te), write(Out, '.'), nl(Out)
. 
