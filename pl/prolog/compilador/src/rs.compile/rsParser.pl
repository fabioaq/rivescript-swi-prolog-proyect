/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

:- module(rsParser, [parse/2]).

:-  use_module(rsLexer).
:-  use_module(rsIndexer).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Main Test %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
testParser(P) :-
    File = 'compilador/cases/micro.rive',
    format('~n~n*** Parsing file: ~s ***~n~n', File),
    parse(File, P),
    line_number(LN),
    format('File ~s parsed: ~d lines processed~n~n', [File, LN])
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Main Parse predicate %%%%%%%%%%%%%%%%%%%%%%%
parse(_, _) :- reset_line_number,
               reset_some_indexes([star, hash, underscore]),
               fail
.
parse(Stream, ProgAst) :-
    tokenize(Stream, Tokens),
    writeln(Tokens),
	rsProgram(ProgAst, Tokens, []),
	!
.
parse(File, _) :-
    Msg = 'Parsing fails. File: ~s. Last Seen Line Number: ~d',
    line_number(N),
    format(atom(A), Msg, [File, N]),
    throw(syntaxError(A, ''))
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Rs Program Grammar %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rsProgram(rsProg(FL)) --> rsCommandList(FL)
.
rsCommandList([]) --> []
.
rsCommandList(L) --> ['\n'], {inc_line_number}, rsCommandList(L)
.
rsCommandList([ TB | R ]) --> topic_block(TB), !, rsCommandList(R)
.
rsCommandList([ trigger_block(TB) | R ]) --> trigger_block(TB), !, rsCommandList(R)
.
rsCommandList([define_block(B) | R])  --> define_block(B), !, rsCommandList(R)
.
%------------------------------ Topic Block
topic_block(topic(T, B)) --> ['>', 'topic'], topic_pattern(T), topic_body(B), ['<', 'topic', '\n']
.

topic_pattern(T) --> id(T), ['\n'], {inc_line_number}
.
topic_pattern(T) --> ['\n'], {inc_line_number}, topic_pattern(T)
.

topic_body([TB | R]) --> trigger_block(TB), topic_body(R)
.
topic_body(TB) --> ['\n'], topic_body(TB)
.
topic_body([]), ['<', 'topic', '\n'] --> ['<', 'topic', '\n']
.
% ----------------------------- Trigger Block
trigger_block( trigger(TL , response_block(RB)) )--> ['+'], pattern(TL), !, response_block(RB)
.
% ---------------------------- Response Block
response_block(RB) --> response_line(RB)
.

response_line([response(R) | RL]) --> ['-'], response_pattern(R), !, response_line(RL)
.
response_line([conditional(T) | RL]) --> ['*'], conditional_pattern(T), !, response_line(RL)
.
response_line([]), [X] --> [X], {is_Terminator(X)}
.
response_line([]) --> []
.
%----------------------------- CONDITIONAL

conditional_pattern([Q | A]) --> test_pattern(Q), ['=>'], response_pattern(A)
.

% ---------------------------- TEST PATTERN

test_pattern(test([L, X, R])), ['=>'] --> test_token(L), cond_operator(X), test_token(R), ['=>']
.

test_token(L) --> response_tag(L)
.
test_token(L) --> [T], {convert_value(T, L)}
.

cond_operator(condOper(X)) --> [X], {is_Op_Operator(X)}
.

% ---------------------------- PATTERN
pattern([])  --> ['\n'], {inc_line_number}
.
pattern([T | TL])  --> token(T), pattern(TL)
.

input_ref(input(N)) --> [IW], {atomic(IW), separate(IW, input, N)}
.
reply_ref(reply(N)) --> [IW], {atomic(IW), separate(IW, reply, N)}
.
separate(W, Pref, Num) :- between(1, 9, Num), atomic_list_concat([Pref, Num], W), !
.
separate(W, W, 1)
.

% ---------------------------- RESPONSE PATTERN
response_pattern([])  --> ['\n'], {inc_line_number}
.
response_pattern([T | TL])  --> response_token(T), response_pattern(TL)
.
%------------------------------- ENFORCERS
enforce_integer(N, N, _) :- integer(N), !
.
enforce_integer(A, N, _):- atom_number(A, N).
enforce_integer(A, _, Msg) :- throw(syntaxError(Msg, A))
.
% ------------------------------ TOKEN
token(T) --> wild_card(T)
.
token(T) --> tag(T)
.
token(W) --> word(W)
.
%------------------------------- RESPONSE TOKEN
response_token(T) --> response_tag(T)
.
response_token(W) --> word(W)
.
% ------------------------------ TAG
tag(I) --> ['<'], input_ref(I),  ['>']
.
tag(I) --> ['<'], reply_ref(I),  ['>']
.
tag(get(W)) --> ['<', get], id(W), ['>']
.
tag(weight(I)) --> ['{'], [weight, '=', V], ['}'],
                                  {enforce_integer(V, I, 'Invalid weight'), !}
.
tag(array(inline, WL)) --> ['('], word_list(WL), [')']
.
tag(array(ref, W)) --> ['(', '@'], word(W), [')']
.
tag(W) --> ['{'], word(W), ['}']
.
tag(optional(N)) --> ['['], wild_card(N), [']']
.
tag(optional(N)) --> ['['], word(N), [']']
.
% ------------------------- Response TAG
response_tag(set(W, V)) --> ['<', set], id(W), ['='], id(V), ['>']
.
response_tag(get(W)) --> ['<', get], id(W), ['>']
.
response_tag(formal(star(0))) --> ['<',formal,'>']
.
response_tag(formal(I)) --> ['{',formal,'}'], response_tag(I), ['{','/',formal,'}']
.
response_tag(star(I)) --> ['<'],[S], ['>'], {star_type(S, I)}
.
response_tag(bot(B)) --> ['<', bot], id(B) , ['>']
.
response_tag(env(B)) --> ['<', env], id(B) , ['>']
.

star_type(S, N):- string_chars(S, L), append([s,t,a,r], [C|_], L), atom_number(C, N), !
.
star_type(S, N):- string_chars(S, L), append([s,t,a,r], [], L), N = 1, !
.
star_type(S, _):- throw(syntaxError('Invalid Character In Star', S))
.
% ------------------------- Define Block/Commands
define_block(B) --> ['!'], define_command(B)
.
define_command(var(global, version, V)) --> [version], ['='], [T, '.', O],
                                      {atom_concat(T, '.', V1),atom_concat(V1, O, V2), convert_value(V2, V)}
.
define_command(var(bot, N, V)) --> [var], word(word(N)), ['='], define_value(V)
.
define_command(var(global, N, V)) --> [global], word(word(N)), {reserved_name(N)},
                                      ['='], define_value(V)
.

define_value([]), ['\n'] --> ['\n']
.
define_value([V | R]) --> [T], {convert_value(T, V)}, define_value(R)
.

% ----------------------------- Common Tools
wild_card(star(N)) --> ['*'], {next_index(star, N)}
.
wild_card(hash(N)) --> ['#'], {next_index(star, N)}
.
wild_card(underscore(N)) --> ['_'], {next_index(star, N)}
.
word(word(W)) --> id(id(W))
.
word_list([]), [')'] --> [')']
.
word_list([W]) --> word(W), word_list([]), !
.
word_list([W | WL]) --> word(W), ['|'], !, word_list(WL)
.


idList([]), [')'] --> [')']
.
idList([I])   --> id(I), idList([])
.
idList([I, J | L]) --> id(I), [','], id(J), idList(L)
.

id(id(I)) --> [I], {atomic(I)}
.
operator(oper(O)) --> {member(O, ['+', '*', '-', '/']), !}
.

convert_value(undefined, const(undefined)) :- !
.
convert_value(T, num(N)) :- atom_number(T, N), !
.
convert_value(T, bool(T)) :- member(T, [true, false])
.
convert_value(T, word(T) ) :- atomic(T)
.

available_name(N) :- \+ reserved_name(N)
.
reserved_name('array')
.
reserved_name('debug')
.
reserved_name('global')
.
reserved_name('var')
.
reserved_name('version')
.
reserved_name('undefined')
.
reserved_name(N) :- member(N, ['+', '-', '*', '/', '@', '^']), !
.
reserved_name(N) :- member(N, ['==', 'eq', '!=', 'ne', '<>', '<', '>', '<=', '>=']), !
.

% --------------------------------- Tools
is_Op_Operator(X):- member(X, ['==', 'eq', '!=', 'ne', '<>', '<', '>', '<=', '>=']), !
.
is_Terminator(X) :- member(X, ['+', '!']), !
.
