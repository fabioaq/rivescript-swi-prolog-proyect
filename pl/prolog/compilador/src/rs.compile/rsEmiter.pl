/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/
:- module(rsEmiter, [
                       genCodeToFile/2,
                       genCode/1,
                       genCode/2
                    ]).

:-  use_module(rsExtractor).
:-  use_module(rsSymbolT).

testEmiter :-
    testParser(P),
    genCode(P)
.

% --------------------------------------- ASSERT INTO SYMBOL TABLE (TRIGGER & RESPONSE)
add_symbt_trigger(Y,V):- rsExtractor:trigger_number(T), rsSymbolT:symbol_assert_t(T,Y,V).

add_symbt_response(Y,V):- rsExtractor:trigger_number(T), rsExtractor:response_number(R), rsSymbolT:symbol_assert_r(T,R,Y,V).

% ---------------------------------------TRIGGER, RESPONSE BLOCKS
genCodeToFile(File, RS_Prog) :-
   %writeln(RS_Prog ),
   open(File, write, Out), writeln(Out),
   genCode(Out, RS_Prog), rsExtractor:select_trigger_response(1), rsExtractor:reset_response_number,
   rsSymbolT:select_from_symbt,
   close(Out)
.
genCode(P) :- genCode(user_output, P)
.
genCode(Out, rsProg(L)) :- !,
    get_time(TS),
    format_time(atom(T), '*** Rive Program *** : Generated at: %d/%m/%y %H:%M:%S', TS),

    genCode(Out, comment(T)),

    genCodeList(Out, L)
.

genCode(Out, define_block(D)):- !, format(Out, '!', []), genVar(Out, D), nl(Out)
.

genCode(Out,trigger_block(T)) :- !,
    nl(Out),
    genCode(Out, comment('>>> Trigger Block')),
    genCode(Out, T)
.
genCode(Out, trigger(WL, R)) :- !,
     rsExtractor:inc_trigger_number,
     genCode(Out, comment('>>> Trigger')),
     format(Out, '+ ', []),
     genCodeList(Out, WL),
     nl(Out),
     genCode(Out, R),
     nl(Out),
	 rsExtractor:trigger_assert(WL)
.

genCode(Out,response_block(R)) :- !,
	rsExtractor:reset_response_number,
    genCode(Out, comment('>>> Response Block')),
    genResponseList(Out, R)
.
genCode(Out, response(WL)) :- !,
	rsExtractor:inc_response_number,
     genCode(Out, comment('>>> Response')),
     format(Out, '- ', []),
     genCodeListResponse(Out, WL),
     rsExtractor:response_assert(WL),
     rsExtractor:trigger_number(TN),
     rsExtractor:r_x_t_assert(TN)
     
.
genCode(Out, conditional([T | R])):- !, rsExtractor:inc_response_number,
                                  format(Out, '* ', []),
                                  genTest(Out, T),
                                  format(Out, ' => ', []),
                                  genCodeListResponse(Out, R)
.
%-----------------------------------------------TOPIC
genCode(Out, topic(id(N), L)):- !, format(Out, '> topic ~a', N),
                            nl(Out), genTriggerList(Out, L),
                            nl(Out), format(Out, '< topic', []),
                            nl(Out)
.
% ----------------------------------------------TRIGGER REPRESENTATION
genCode(Out, hash(S)) :- !, genCode(Out, atom('#')), add_symbt_trigger(hash,S)
.
genCode(Out, star(S)) :- !, genCode(Out, atom('*')), add_symbt_trigger(star,S)
.
genCode(Out, underscore(S)) :- !, genCode(Out, atom('_')), add_symbt_trigger(underscore,S)
.
genCode(Out, weight(I)) :- !, format(Out,'{weight=~d} ',I), add_symbt_trigger(weight,I)
.
genCode(Out, word(N)) :- !, genCode(Out, atom(N))
.
genCode(Out, id(N)) :- !, genCode(Out, atom(N))
.
genCode(Out, num(N))  :- !, genCode(Out, atom(N))
.
genCode(Out, oper(N)) :- !, genCode(Out, atom(N))

.
genCode(Out, get(id(N))) :- !, format(Out,'<get ~a> ',N), add_symbt_trigger(get,N)
.
genCode(Out, optional(O)) :- !,  genCode(R, O), format(Out, '[~a] ', R), add_symbt_trigger(optional,O)
.

% ------>   INTERNAL REPRESENTATION
genCode(Out, operation(O, L, R)) :- !,
    genCodeList(Out, [L, O, R])
.
genCode(Out, atom(N)) :- !, format(Out, '~a ', [N])
.
genCode(Out, comment(C)):-
     format(Out, '// ~a \n', [C])
.
% ------>   ERROR CASE
genCode(Out, E ) :- close(Out),!,
                    throw(genCode('genCode unhandled Tree', E))
.

genVar(Out, var(global, version, word(V))) :- !, format(Out, 'version=~a', V)
.
genVar(Out, var(bot, word(N), word(V))) :- !, format(Out, 'var ~a=~a', [N,V])
.
genVar(Out, var(global, word(N), word(V))) :- !, format(Out, 'global ~a=~a', [N,V])
.

genVar(Out, word(N)) :- !, genCode(Out, atom(N))
.

% ----------------------------------------------RESPONSE REPRESENTATION
genCodeResponse(Out, set(I, E)) :-  !,
   genCode(Out, operation(oper('='), I, E))
.
genCodeResponse(Out, word(N)) :- !, genCode(Out, atom(N))
.
genCodeResponse(Out, id(N)) :- !, genCode(Out, atom(N))
.
genCodeResponse(Out, num(N))  :- !, genCode(Out, atom(N))
.
genCodeResponse(Out, oper(N)) :- !, genCode(Out, atom(N))
.
genCodeResponse(Out, bot(B)) :- !, format(Out,'<bot ~a> ',B), add_symbt_response(bot,B)
.
genCodeResponse(Out, formal(star(1))) :- !, format(Out, '<formal> ', []), add_symbt_response(formal,1)
.
genCodeResponse(Out, formal(N)) :- !, genCodeResponse(FormIn, N), format(Out,'{formal}~s{/formal} ', FormIn), add_symbt_response(formal,N)
.
genCodeResponse(Out, star(1)) :- !, format(Out, '<star> ', []), add_symbt_response(star,1)
.
genCodeResponse(Out, star(N)) :- !, format(Out, '<star~d> ', N), add_symbt_response(star,N)
.
genCodeResponse(Out, env(E)) :- !, format(Out,'<env ~a> ',E), add_symbt_response(env,E)
.
genCodeResponse(Out, get(id(N))) :- !, format(Out,'<get ~a> ', N), add_symbt_response(var,N)
.
genCodeResponse(Out, const(N)) :- !, format(Out,' ~a ',N), add_symbt_response(const,N)
.
% -----------------------------------------------TEST
genTest(Out, test([L, O, R])):- !, genCodeResponse(Out, L), genTest(Out, O), genCodeResponse(Out, R)
.
genTest(Out, condOper(O)):- !, format(Out, ' ~a ', [O]), add_symbt_response(cond,O)
.
% -----------------------------------------------CODE LISTS
genCodeList(Out, L) :- genCodeList(Out, L, '')
.
genCodeList(_, [], _).
genCodeList(Out, [C], _) :- genCode(Out, C).
genCodeList(Out, [X, Y | L], Sep) :- genCode(Out, X),
                                     format(Out, '~a', [Sep]),
                                     genCodeList(Out, [Y | L], Sep)
.


genCodeListResponse(Out, L) :- genCodeListResponse(Out, L, '')
.
genCodeListResponse(_, [], _).
genCodeListResponse(Out, [C], _) :- genCodeResponse(Out, C).
genCodeListResponse(Out, [X, Y | L], Sep) :- genCodeResponse(Out, X),
                                     format(Out, '~a', [Sep]),
                                     genCodeListResponse(Out, [Y | L], Sep)
.
genCodeListTrigger(Out, L) :- genCodeListTrigger(Out, L, '')
.
genCodeListTrigger(_, [], _).
genCodeListTrigger(Out, [C], _) :- genCode(Out, C).
genCodeListTrigger(Out, [X, Y | L], Sep) :- genCode(Out, X),
                                     format(Out, '~a', [Sep]),
                                     genCodeListTrigger(Out, [Y | L], Sep)
.

genResponseList(Out, L) :- genResponseList(Out, L, '\n')
.
genResponseList(_, [], _).
genResponseList(Out, [C], _) :- genCode(Out, C).
genResponseList(Out, [X, Y | L], Sep) :- genCode(Out, X),
                                     format(Out, '~a', [Sep]),
                                     genResponseList(Out, [Y | L], Sep)
.
genTriggerList(Out, L) :- genTriggerList(Out, L, '\n')
.
genTriggerList(_, [], _).
genTriggerList(Out, [C], _) :- genCode(Out, C).
genTriggerList(Out, [X, Y | L], Sep) :- genCode(Out, X),
                                     format(Out, '~a', [Sep]),
                                     genTriggerList(Out, [Y | L], Sep)
.
