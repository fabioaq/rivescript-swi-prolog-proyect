/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Line number %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(rsLexer, [tokenize/2, 
                    reset_line_number/0, 
                    inc_line_number/0,
                    line_number/1
                    ]).
/** <module> Prolog Lexer (Tokenizador)
 * 
 * Este módulo se encarga de la tokenización de un stream de formato .rive
 */

:- dynamic line_number/1
.

/**
 * reset_line_number
 *
 * No recibe parametros, limpia line_number el cual es un contador de lineas 
 */
reset_line_number :- retractall(line_number(_)), inc_line_number
.

/**
 * inc_line_number
 *
 * No recibe parametros, incrementa el numero de linea del contador  
 */
inc_line_number   :- retract(line_number(N)), 
                     N1 is N + 1, 
                     assert(line_number(N1)), !
.
inc_line_number   :- assert(line_number(1))
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Main %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**
 * codes_to_chars(+Lcodes:List,-Lchars:List)
 *
 * Recibe Lcodes lista de codigos y devuelve Lchars lista de chars
 * convierte una lsita de codigos a chars.   
  */

codes_to_chars(Lcodes, Lchars):-
    atom_codes(Atom_from_codes, Lcodes),
    atom_chars(Atom_from_codes, Lchars).

% se cambia el File por un Stream  

/**
 * tokenize(+S:Stream, -Tokens:List)
 *
 * Recibe un S -> stream de caracteres y lo devulve en Tokens como una lista.
 */
tokenize(S, Tokens) :-
                          read_stream_to_codes(S, Codes),
                          codes_to_chars(Codes, Input),
                          reset_line_number,
                          getTokens(Input, Tokens)
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Test %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% testLexer(L) :-
    %write(D),
    % File = '\tExample  Text.',
    % format('~n~n*** Lexing file: ~s ***~n~n', File),
    % tokenize(File, L),
    % line_number(LN),
    % format('File ~s lexed: ~d lines processed~n~n', [File, LN])
% .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Get Tokens %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/**
 * getTokens(+Input:List, -Tokens:List)
 *
 * Obtiene los tokens y extrae los tokens nulos.
 */
getTokens(Input, Tokens) :- extractTokens(Input, ExTokens),
                            delete(ExTokens, [], Tokens) % delete null ('') tokens
.

/**
 * extractTokens(+Input:List, -ExtractedTokens:List)
 *
 * Extrear uno por uno los tokens delimitados por un espacio en blanco.
 */
extractTokens([], []) :- !.
extractTokens(Input, [Token | Tokens]) :-  skipWhiteSpace(Input, InputNWS),
                                           startOneToken(InputNWS, Token, Rest),
                                           extractTokens(Rest, Tokens)
.

/**
 * skipWhiteSpace(+Input:List, -Output:List)
 * 
 * Elimina los espacion en blancoy saltos de linea.
 */
skipWhiteSpace([C | Input], Output) :- isWhiteSpace(C), !, 
                                       skipWhiteSpace(Input, Output)
.

skipWhiteSpace(Input, Input)
.

/**
 * startOneToken(+Input:List, -Token:List, -Rest:List)
 *
 * convierte la lista de tokens entrnate en una lista de tokens definidos.
 */
startOneToken(Input, Token, Rest) :- startOneToken(Input, [], Token, Rest)
.
startOneToken([], P, P, [])
.
startOneToken([C | Input], _, '\n', Input)       :- isNewLine(C), inc_line_number, !
.
startOneToken([C | Input], Partial, Token, Rest) :- isDigit(C), !,
                                                    finishNumber(Input, [ C | Partial], Token, Rest)
.

startOneToken([C | Input], Partial, Token, Rest) :- isLetter(C), !,
                                                    finishId(Input, [ C | Partial], Token, Rest)
.

startOneToken([C | Input], Partial, Token, Rest) :- isOper(C), !,
                                                    finishOper(Input, [ C | Partial], Token, Rest)
.
startOneToken([C | Input], Partial, Token, Rest) :- isQuote(C), !,
                                                    finishQuote(Input, [ C | Partial], Token, Rest)
.
startOneToken([C | _] , _, _, _) :- report_invalid_symbol(C)
.

/**
 * report_invalid_symbol(+C:String)
 *
 * generea un error con el atomo C recibido.
 */
report_invalid_symbol(C) :-
    Msg='*** Lexer Error: Unexpected symbol "~s" found in input stream. Line Number ~d ***',
    line_number(LN),
    format(atom(A), Msg, [C, LN]),
    throw(lexerError(A, ''))
.
report_invalid_string(Msg) :-
    Msg='*** Lexer Error: Unclosed string in input stream. Line Number ~d ***',
    line_number(LN),
    format(atom(A), Msg, [LN]),
    throw(lexerError(A, ''))
.

/**
 * finishNumber(Input, Partial, Token, Rest)
 * 
 * Generea un error con el atomo C recibido.
 */
finishNumber(Input, Partial, Token, Rest) :- finishToken(Input, isDigit, Partial, Token, Rest)
.
finishId(Input, Partial, Token, Rest) :- finishToken(Input, isAlpha, Partial, Token, Rest)
.
% FINISH QUOTE
finishQuote([C | Input], Partial, Token, Input) :- isQuote(C), !,
                                                   convertToAtom([C | Partial], Token)
.
finishQuote([C | Input], Partial, Token, Rest) :- finishQuote(Input, [C |Partial], Token, Rest)
.
finishQuote([] , _Partial, _Token, _Input) :- report_invalid_string('opened and not closed string')
.
% EXTRACT OPER
finishOper([C | Input], [PC | Partial], Token, Input) :- doubleOper(PC, C), !,
                                                         convertToAtom([C, PC | Partial], Token)
.
finishOper(Input, Partial, Token, Input) :- convertToAtom(Partial, Token)
.
% FINISH TOKEN
finishToken([C | Input], Continue, Partial, Token, Rest) :- call(Continue, C), !,
                                                            finishToken(Input, Continue, [ C | Partial], Token, Rest)
.

finishToken(Input, _, Partial, Token, Input) :- convertToAtom(Partial, Token)
.

% CONVERT into TOKEN
convertToAtom(Partial, Token) :- reverse(Partial, TokenCodes),
                                 atom_codes(Token, TokenCodes)
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHARACTER CLASSES (ONLY ASCII)

/**
 * isNewLine(+C:Atom)
 *
 * Recibe un C -> detecta si es si es un cambio de linea.
 */
isNewLine(C) :- member(C, ['\n', '\r']).

/**
 * isWhiteSpace(+C:Atom) 
 * 
 * devuelve un true si C es un tabulador o un espacio en blanco.
 */
isWhiteSpace(C) :- member(C, ['\t', ' ']).

/**
 * isLetter(+D:Atom) 
 * 
 * devuelve un true si D es un numero entre 0 y 9.
 */
isDigit(D)   :- D @>= '0', D @=< '9'.

/**
 * isLetter(+D:Atom) 
 * 
 * devuelve un true si D es una letra y falla si es '$' o un '_'.
 */
isLetter('_') :- !. 
isLetter('$') :- !. 
isLetter(D)  :- D @>='a', D @=< 'z', !.  % a .. z
isLetter(D)  :- D @>= 'A', D @=< 'Z'.    % A .. Z

/**
 * isAlpha(+D:Atom) 
 * 
 * devuelve un true si D es una numero o una letra.
 */
isAlpha(D) :- isLetter(D);isDigit(D).

/**
 * isQuote(+A:Atom) 
 * 
 * devuelve un true si A es una '"' (comilla doble).
 */
isQuote('"'). 

/**
 * isBackSlash(+A:Atom) 
 * 
 * devuelve un true si A es un '\' (slash).
 */
isBackSlash('\\')          % \
.
isOper(O)    :- member(O, ['=', '<', '>', '*', '-', '+', '/', '\\', '.', '(', ')', '\'']), !.
isOper(O)    :- member(O, ['{', '}', '&', '|', '%', '!', ';', ',', '#', '?','[',']']), !.
isOper(O)    :- member(O, ['@', ':']), !.

/**
 * doubleOper(+O:Atom,+B:Atom).
 * 
 * devuelve un true si O y B conicide con alguno de los operadores dobles.
 */
doubleOper('!', '='). % !=
doubleOper('=', '='). % ==
doubleOper('<', '='). % <=
doubleOper('>', '='). % >=
doubleOper('<', '>'). % <>
doubleOper('=', '>'). % =>
