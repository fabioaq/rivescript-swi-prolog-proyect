/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

:- module(rsExtractor, [reset_trigger_number/0, 
                    reset_response_number/0,
                    inc_trigger_number/0,
					inc_response_number/0,
					trigger_assert/1,
					response_assert/1,
					select_from_response/0,
					select_from_trigger/0,
					pk_trigger/1,
					pk_response/1,
					select_trigger_response/1
                    ]).

% -------------------------------------RESETS
reset_trigger_number :- retractall(trigger_number(_))
.

reset_response_number :- retractall(response_number(_))
.

% -------------------------------------KEYS
% PRIMARY KEY FOR TRIGGER
inc_trigger_number   :- retract(trigger_number(N)), 
                     N1 is N + 1, 
                     assert(trigger_number(N1)), !
.
inc_trigger_number   :- assert(trigger_number(1))
.

% PRIMARY & FOREING KEY FOR RESPONSE
inc_response_number   :- retract(response_number(N)), 
                     N1 is N + 1, 
                     assert(response_number(N1)), !
.
inc_response_number   :- assert(response_number(1))
.

% ------------------------------------ASSSERTS
% TRIGGER ASSERT assert(trigger(pk, data)) pk= trigger_number
trigger_assert(WL):- trigger_number(T), assert(trigger(T,WL))
.
% RESPONSE ASSERT assert(response(fk, pk, data)) fk= trigger_number pk= response_number
response_assert(WL):- trigger_number(FK), response_number(PK), assert(response(FK,PK,WL))
.
% RESPONSE CONDITIONAL assert(response(fk, pk, test, data))
%response_condition_assert(T, WL):- trigger_number(FK), response_number(PK), assert(response(FK,PK,T,WL))
%.
% cantidad responses por trigger TK= trigger_number======================================= NUEVOS METODOS
r_x_t_assert(TK):- retract(cant_R_x_T(TK,_)),
 	response_number(RK),
	assert(cant_R_x_T(TK,RK)).

r_x_t_assert(TK):- response_number(RK),
	assert(cant_R_x_T(TK,RK)).
% ======================================================================================== FIN NUEVOS METODOS

% ------------------------------------API
% select all triggers
select_from_trigger:- findall([N,M],(trigger(N,M)), R), writeln(R).

% select all responses
select_from_response:- findall([Z,N,M],(response(Z,N,M)), R), writeln(R).

% select trigger and it responses where PK is
pk_trigger(PK):- findall([PK,M],(trigger(PK,M)), R), writeln(R).
pk_response(PK):- findall([PK,N,M],(response(PK,N,M)), R), writeln(R).
select_trigger_response(PK):- pk_trigger(PK), pk_response(PK).

% ======================================================================================== NUEVOS METODOS
select_cant_R_X_T(R):-  findall(cant_R_x_T(N,M),(cant_R_x_T(N,M)), R), writeln(R).
all_trigger(R):- findall(trigger(PK,M),(trigger(PK,M)), R), writeln(R).
all_response(R):- findall(response(PK,N,M),(response(PK,N,M)), R), writeln(R).

%oooooooooooooooooooooooooooooooooooooooooooooooooo
delete_word:- 
	all_trigger(R),delete_word_Trigger(R),
 	all_response(L), delete_word_Response(L)
. 

delete_word_Trigger([]).
delete_word_Trigger([trigger(A,M)|B]):-
	cambiador_word(M,W),
	delete_word_Trigger(B),
	retract(trigger(A,_)),
	assert(trigger(A,W)),
	writeln('Triggers: ' + W)
. 

delete_word_Response([]).
delete_word_Response([response(A,Z,M)|B]):-
	cambiador_word(M,W),
	delete_word_Response(B),
	retract(response(A,Z,M)),
	assert(response(A,Z,W)),
	writeln('Responses ' + W)
. 

cambiador_word([],[]).
cambiador_word([word(H)|B],RL):- 
	format(atom(Out), '\'~a\'', [H]),
	cambiador_word(B,RL1),
	append([Out], RL1, RL)
. 
cambiador_word([H|B],RL):- 
	cambiador_word(B, RL1),
	append([H], RL1, RL).