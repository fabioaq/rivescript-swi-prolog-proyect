/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

:- module(rsSymbolT, [symbol_assert_t/3,
					symbol_assert_r/4,
					select_from_symbt/0
                    ]).
% estos son diccionarios, TRIGGER y RESPONSE respectivamente
symbol_assert_t(T,Y,V):- assert(symbol_table({trigger: T, type: Y, value: V, resolve: null})).

symbol_assert_r(T,R,Y,V):- assert(symbol_table({trigger: T, response: R, type: Y, value: V, resolve: null})).

% esto imprime todo lo que está dentro del diccionario symbol_table
select_from_symbt:- current_predicate(symbol_table/1),findall(ST,(symbol_table(ST)), R), writeln(R).
select_from_symbt:- writeln(0).