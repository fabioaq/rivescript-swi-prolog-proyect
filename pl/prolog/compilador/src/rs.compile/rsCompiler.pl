/*
------Lisandra Agüero Ruiz
------Esteban Espinoza Fallas
------Steve Díaz Reyes
------José Fabio Alfaro Quesada
------2019
*/

:- module(rsCompiler, [compile/0, compile/1, compile/3]).

:- use_module(rsParser).
:- use_module(rsEmiter).
:- use_module(rsGenerator).

% el predicado compile/3, enviará a rsParser:parse el BotBody como Stream
% y a rsEmiter:genCodeToFile la ruta donde se guadará el nuevo bot y el AST
compile(BotBody, OutPath, BotName) :- !,
   rsParser:parse(BotBody, P), % se envía al Parser
   atom_concat('./rive/', BotName, PathOutFile), % nombre del bot
   atom_concat(PathOutFile, '.rive', RSOutFile), % concateno el .out
   format('*** Writing   :"~a" *** ~n', [RSOutFile]),
   rsEmiter:genCodeToFile(RSOutFile, P), % se envía al Emiter
   %para generar el archivo para evaluar
   atom_concat(OutPath, BotName, PathOutCompiledFile), % nombre del bot
   atom_concat( PathOutCompiledFile, '.out.pl', RSOutCompiledFile), % concateno el .out
   format('*** Writing   :"~a" *** ~n', [RSOutCompiledFile]),
   rsGenerator:genRiveToFile(RSOutCompiledFile)
.

compile(InPath, _, Filename) :-
   atom_concat(InPath, Filename, PathInFile),
   format('*** RSCompiler: File Not found :"~a" *** ~n', [PathInFile]),
   fail
.
compile(Filename) :- compile('./cases/', './output/', Filename)
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Default test case %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
compile :- 
    File = 'micro.rive',
    compile(File)
.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Entry Point %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
main :-
    writeln('*** Starting compilation ***'),
    current_prolog_flag(argv, AllArgs),
    [E|_] = AllArgs,
    compile(E),
   writeln('*** Sucessful compilation ***'), !
.
main :-
    writeln('*** Provide an existing test case file ***')
.