/**
 * @file Archivo js/rivescript
 * @author Lisandra Agüero Ruiz
 * @author Esteban Espinoza Fallas
 * @author Steve Díaz Reyes
 * @author José Fabio Alfaro Quesada 
 * @name script.js
 */

/**
 * Bot interactivo
 * @type {string}
 */
let bot;
/**
 * Archivo .rive que se utilizará para las respuestas
 * @type {string}
 */
var rivet_file;
/**
 * variable que almacena la dirección de la foto del bot seleccionado
 * @type {string}
 */
let foto_chat;
/**
 * Nombre del bot a usar
 * @type {string}
 */
let nombre_bot;
/**
 * socket que envia los mensajes al server-sockets para mensajeria
 * @type {string}
 */
let exampleSocket;

/**
 * socket que envia los mensajes al server-sockets para login
 * @type {string}
 */
let login_socket = new WebSocket('ws://localhost:3000/login');

/**
 * socket que envia los archivos al server-sockets para login
 * @type {string}
 */
let file_socket = new WebSocket('ws://localhost:3000/files');

// -----------------Inicializar inbox_chat con bots 

/**
 * inicializa el cuadro de chats apra poder hablar con lso bots 
 */
function initChat() {
    setTimeout(function() { // Evita error al esperar carga del socket
        file_socket.send(' ');
        file_socket.onmessage = function(event) {
            let array = (JSON.parse(event.data)).map(function(x) {
                return x.replace(/.rive/g, '');
            });
            divchat(array);
        }
    }, 2000);
}

/**
 * socket que envia los mensajes al server-sockets para login
 * @param {Array} array  array con el nombre de los bots cargadso desde el server_sokets
 */
function divchat(array) {
    array.forEach(function(e) {
        var img = "img\\" + e + ".jpg";
        $('.inbox_chat').append(
            "<div class='chat_list'><div class='chat_people' onclick='select_bot_speak(\"" + e + "\")' name='" +
            e + "'><div class='chat_img'> <img src='" +
            img + "' alt='sunil' class='rounded-circle'></div><div class='chat_ib'><h5>" +
            e + "</h5></div></div></div>"
        );
    });
}

/**
 * socket que envia los mensajes al server-sockets para login
 * @type { WebSocket}
 */
let brain_socket = new WebSocket('ws://localhost:3000/test');

document.getElementById("sendsave").onclick = function() {
    let brain_name = document.getElementById("nombre").value;
    let brain_body = document.getElementById("Textarea1").value;

    if (brain_name.length < 0 || brain_name == "") {
        errorBox("El nombre se encuentra vacío");
        return;
    }
    if (brain_body.length < 0 || brain_body == "") {
        errorBox("El bloque se encuentra vacío");
        return;
    }

    let json = {
        'brain_body': brain_body + "\n" + "\n",
        'brain_name': brain_name
    };

    brain_socket.send(JSON.stringify(json));
    brain_socket.onmessage = function(event) {
        console.log(event.data);
        $('.chat_list').remove();
        initChat();
        successBox("El bot " + brain_name + " ha sido guardado!")
        $('#nombre').val('');
        $('#Textarea1').val('');
    };

};

/**
 * metodo para mostrar un error
 * @param {string} mensaje  mensaje a mostrar
 */
function errorBox(message) { //para errores
    bootbox.alert({
        size: 'small',
        closeButton: false,
        message: '! ' + message
    });

}

/**
 * metodo para mostrar aceptado
 * @param {string} mensaje  mensaje a mostrar
 */
function successBox(message) { //para éxito
    bootbox.alert({
        size: 'small',
        closeButton: false,
        message: message
    });

}

/**
 * selecciona el archivo del bot para hablar y desbloquea el chat para ese usuario y asi pueda hablar con el bot cliqueado
 * @param {string} nombre  nombre del bot
 */
function select_bot_speak(name) {
    nombre_bot = name
    $('.chat_people').removeClass('active_chat');
    $('[name="' + name + '"]').addClass('active_chat')
    $('#actual').empty();
    seleccionado(name);
    $('#chatico').show();
}


$('#admininterface').click(function() {
    rivet_file = "";
    $('.chat_people').removeClass('active_chat');
    $('#userinterface').removeClass('active');
    $('#admininterface').addClass('active');
    $('#chatico').hide();
    $('#adminint').show();
})

$('#userinterface').click(function() {
    rivet_file = "";
    $('.chat_people').removeClass('active_chat');
    $('#admininterface').removeClass('active');
    $('#userinterface').addClass('active');
    $('#actual').empty();
    $('#chatico').hide();
    $('#adminint').hide();
})



login_socket.onmessage = function(evt) {
    console.log(evt.data);
    let obj = JSON.parse(evt.data);
    if (obj.status === "OK") {
        $("#nomuserlog").text(obj.data.nombre);

        if (obj.data.rol === 1) {
            interfazadmin();
            $("#roluserlog").text("Administrador");
        } else {
            interfazuser();
            $("#roluserlog").text("Usuario");
        }
        $('#iduser').val('');
        $('#passuser').val('');
        $('#sinlogin').hide();
        $('#conlogin').show();
        $('#iduser').hide();
        $('#passuser').hide();
        $('#loginuser').hide();
        $('#logoutuser').show();
        $('#userlogged').show();
        successBox(obj.msg);
    } else {
        errorBox(obj.msg + "usuario y contraseña incorrectos");
        $('#iduser').val('');
        $('#passuser').val('');
    }

}

$('#loginuser').click(function() {
    if (!$('#iduser').val() || !$('#passuser').val()) {
        errorBox("Error ingrese: \n usuario y contraseña");
    } else {
        let id = $('#iduser').val();
        let pass = $('#passuser').val();
        let json = {
            'id': id,
            'pass': pass
        };
        login_socket.send(JSON.stringify(json));
        $('#actual').empty();
    }
})

$('#logoutuser').click(function() {
    $('#sinlogin').show();
    $('#conlogin').hide();
    $('#userlogged').hide();
    $('#loginuser').show();
    $('#logoutuser').hide();
    $('#iduser').show();
    $('#passuser').show();
    $('#userinterface').hide();
    $('#admininterface').hide();
})

/**
 * activa y desactiva partes de la interfaz para el usuario administrador
 */
function interfazadmin() {
    $('#chatico').hide();
    $('#adminint').show();
    $('#userinterface').hide();
    $('#admininterface').show();
}

/**
 * activa y desactiva partes de la interfaz para el usuario chater
 */
function interfazuser() {
    $('#chatico').hide();
    $('#adminint').hide();
    $('#userinterface').show();
    $('#admininterface').hide();
}

/**
 * Devuelve la direccion de la foto del chat seleccionado
 * @param {string} nombre - Nombre de la imagen.
 */
function seleccionado(nombre) {
    switch (nombre) {
        case 'esteban':
            foto_chat = "img\\Esteban.jpg";
            break;
        case 'fabio':
            foto_chat = "img\\Fabio.jpg";
            break;
        case 'steve':
            foto_chat = "img\\steve.jpg";
            break;
        case 'lis':
            foto_chat = "img\\Lis.jpg";
            break;
        default:
            foto_chat = "img\\patricio.jpg";
    }
}

/**
 * Recupera el msj escrito por el usuario para luego ser enviado por send()
 * y muestra el msj escrito en el html 
 */
function mensaje() {
    var msj_enviar = $("#campoTexo").val();
    if (msj_enviar) {
        send(msj_enviar);
        $("#campoTexo").val("");
        $('#actual').append(burbuja_chat("", msj_enviar, "user"))
    }
}

/**
 * Recupera el msj escrito por el usuario para luego ser enviado por send()
 * @param {string} imagen url de la imagen a mostrar
 * @param {string} mensaje mensaje recibido o enviado del o para le bot
 * @param {string} remitente tipo de entidad que manda el mensaje peude se usuario o bot
 */
function burbuja_chat(imagen, mensaje, remitente) {
    let hora = clock();
    //abajo
    if (remitente === "bot") {
        var emphasis = "<div class=\"incoming_msg\"><div class=\"incoming_msg_img\"> <img src=\"" + imagen + "\"alt=\"sunil\"> </div>" +
            "<div class=\"received_msg\"><div class=\"received_withd_msg\"><p>" + mensaje + "</p>" +
            "<span class=\"time_date\">" + hora + "</span></div></div></div>";
    } else {
        var emphasis = "<div class=\"outgoing_msg\"><div class=\"sent_msg\"><p>" + mensaje + "</p><span class=\"time_date\">" + hora + "</span></div></div>";
    }

    return emphasis;
}

/**
 * Metodo ejecutado cuando se carga el html, iniciliza el socket con sus metodos
 */
function init_socket() {
    exampleSocket = new WebSocket('ws://localhost:3000/prueba');
    initChat();
    $('#conlogin').hide();
    $('#userinterface').hide();
    $('#admininterface').hide();
    $('#logoutuser').hide();
    $('#userlogged').hide();
    exampleSocket.onmessage = function(evt) {
        onMessage(evt)
    };
}

/**
 * Se realiza el envio del msj por medio del socket al server-sockets con un formato JSON
 * @param {string} message mensaje a enviar
 */
function send(message) {
    var mensaje = {
        "username": nombre_bot,
        "msg": message
    };
    exampleSocket.send(JSON.stringify(mensaje));
}

/**
 * Se realiza el envio del msj por medio del socket al server-sockets con un formato JSON
 * @param {string} evt evento llega del request
 */
function onMessage(evt) {
    let msj_to_screen = evt.data.substring(1, evt.data.length - 1)
    console.log("DATA:" + msj_to_screen);
    $('#actual').append(burbuja_chat(foto_chat, msj_to_screen, "bot"));
}

// =================================================== RELOJ ===================================================

/**
 * actualiza segundo a sengundo el reloj
 */
setInterval(function() {
    var date = new Date();
    clock();
    updateClock(date);
}, 1000);

/**
 * actualiza el widget del relog segun la hora indicada en el date
 * @param {Date} date fecha del sistema
 */
function updateClock(date) {
    var secHand = document.getElementById("sec-hand").style;
    var minHand = document.getElementById("min-hand").style;
    var hrHand = document.getElementById("hr-hand").style;

    secHand.transform = "rotate(" + date.getSeconds() * 6 + "deg)";
    minHand.transform = "rotate(" + date.getMinutes() * 6 + "deg)";
    hrHand.transform = "rotate(" + (date.getHours() * 30 + date.getMinutes() * 0.5) + "deg)";
}

/**
 * agrega un formato de 0 en caso de ser numeros menores a 10
 * @param {integer} s numero que desea formatear
 */
function addZero(s) {
    return (s.length < 2) ? ("0" + s) : s;
}

/**
 * devulve una hora 
 */
function clock() {
    let date = new Date();
    let time = [date.getHours(), date.getMinutes(), date.getSeconds()]
        .map(function(s) {
            return addZero(s.toString());
        })
        .join(":");
    return time;
}

$(document).ready(function() {
    $("#campoTexo").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            mensaje();
        }
    });
});

window.addEventListener("load", init_socket, false);