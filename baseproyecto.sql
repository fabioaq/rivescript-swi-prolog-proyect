create database proyecto;

-- tabla de usuarios
CREATE TABLE `proyecto`.`usuarios` (
  `id` INT NOT NULL,
  `pass` VARCHAR(45) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `rol` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- usuarios de chat
INSERT INTO `proyecto`.`usuarios` (`id`, `nombre`, `pass`, `rol`) VALUES ('116630669', 'Lisandra', '1111','2');
INSERT INTO `proyecto`.`usuarios` (`id`, `nombre`, `pass`, `rol`) VALUES ('207580494', 'J. Fabio', '2222','2');
INSERT INTO `proyecto`.`usuarios` (`id`, `nombre`, `pass`, `rol`) VALUES ('116040144', 'Steve R.', '3333','2');
INSERT INTO `proyecto`.`usuarios` (`id`, `nombre`, `pass`, `rol`) VALUES ('402290345', 'Esteban', '4444','2');

-- usuario adminsitrador
INSERT INTO `proyecto`.`usuarios` (`id`, `nombre`, `pass`, `rol`) VALUES ('111111111', 'Carlos', '666','1');